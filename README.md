# Quantum Computing Lecture, MCnet summer school 2023, Durham, UK

Hi there! This is a repository of my Quantum Computing lecture at the [ MCnet school 2023 ](https://conference.ippp.dur.ac.uk/event/1179/). If you are just interested in looking at the slides, just clone the repository and go for it. For running the code cells, see below.

## Usage

The lecture slides are written as jupyter notebook, and will be presented using the amazing [ RISE ](https://rise.readthedocs.io/en/stable/) jupyter extension.

You can read the slides without pictures online, or clone this repository and open them in jupyter. Below some instructions in case you also want to run the code cells:

- You should have docker installed
- You'll need the open-source [ Quantum Brilliance Qristal SDK ](https://quantumbrilliance.com/quantum-brilliance-qristal) (see also [ here ](https://qristal.readthedocs.io/en/latest/) and [ here ](https://gitlab.com/qbau/software-and-apps/public/) ) docker container, which you can get and run by executing  
`docker run --rm -p 8889:8889 registry.gitlab.com/qbau/software-and-apps/public/qbsdk`  
(NB: depending on which ports are busy on your system, you may need to replace `8889` with `8888`)
- Open `localhost:8889/lab` in a browser. This will open a JuypterLab instance that you can use as sandbox to start developing quantum algorithms.
- In Jupyterlab, create a new directory (e.g. called `mcnet`)
- In Jupyterlab, open a terminal, navigate to the new directory and clone the repository  
`git clone https://gitlab.com/prestel/mcnet-qclecture.git`
- Click into the directory and on the `QClecture.ipynb` notebook.
- Have fun coding!

## Acknowledgement

Thanks to [ Quantum Brilliance ](https://quantumbrilliance.com/) (especially Nils Herrmann, Daanish Arya, Florian Preis and John L. Helm) and [ MCnet ](https://montecarlonet.org/).
